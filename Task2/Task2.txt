--1.Написать запрос, который возвращает наименование клиентов и кол-во контактов клиентов
SELECT 
c.ClientName,
COUNT(cc.ID) [Count]
FROM Clients AS c
LEFT JOIN ClientContacts AS cc ON c.id = cc.ClientId
GROUP BY c.id, c.ClientName

--2.Написать запрос, который возвращает список клиентов, у которых есть более 2 контактов
SELECT 
c.ClientName,
COUNT(cc.ID) [Count]
FROM Clients AS c
LEFT JOIN ClientContacts AS cc ON c.id = cc.ClientId
GROUP BY c.id, c.ClientName
HAVING COUNT(cc.ID) > 2