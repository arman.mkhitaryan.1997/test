﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task1.Models;
using Task1.Models.EmployeeModel;

namespace Task1.Controllers
{
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        IEmployeeRepository EmployeeRepository;
        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            EmployeeRepository = employeeRepository;
        }

        /// <summary>
        /// Метод получает JSON и сохраняет его в БД
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpPost] //api/Employee
        public IActionResult Create([FromBody] object body)
        {
            if (body == null)
            {
                return BadRequest();
            }
            try
            {
                //Удаляем текущие данные из таблицы
                EmployeeRepository.DeleteAll();
                SortedDictionary<int, string> values = JsonConvert.DeserializeObject<SortedDictionary<int, string>>(body.ToString());

                for (int i = 0; i < values.Count; i++)
                {
                    var item = values.ElementAt(i);
                    Employee employee = new Employee(i + 1, item.Key, item.Value);
                    EmployeeRepository.Create(employee);
                }
                return Ok();
            }

            catch(Exception)
            {
                return BadRequest();
            }
        }

        //api/Employee
        /// <summary>
        /// Метод возвращает данные из таблицы
        /// </summary>
        /// <param name="employeeName"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Employee> Get(string employeeName = null)
        {
            return EmployeeRepository.Get(employeeName);
        }
    }
}
