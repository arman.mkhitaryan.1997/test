﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task1.Models.EmployeeModel
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> Get(string employeeName = null);
        void Create(Employee item);
        void DeleteAll();
    }
}
