﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task1.Models.EmployeeModel
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private AppDbContext Context;
        public EmployeeRepository(AppDbContext context)
        {
            Context = context;
        }
        public void Create(Employee employeeItem)
        {
            Context.Employees.Add(employeeItem);
            Context.SaveChanges();
        }

        public void DeleteAll()
        {
            Context.Employees.RemoveRange(Context.Employees);
            Context.SaveChanges();
        }

        public IEnumerable<Employee> Get(string employeeName = null)
        {
            if(employeeName != null)
            {
                return Context.Employees.Where(x => x.Name == employeeName);
            }
            return Context.Employees;
        }
    }
}
