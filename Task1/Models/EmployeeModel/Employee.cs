﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task1.Models
{
    public class Employee
    {
        /// <summary>
        /// Порядковый номер
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        ///  Код сотрудника
        /// </summary>
        [Key]
        public int Code { get; set; }
        /// <summary>
        /// ФИО сотрудника
        /// </summary>
        public string Name { get; set; }


        public Employee(int number, int code, string name)
        {
            this.Number = number;
            this.Code = code;
            this.Name = name;
        }
    }
}
