import React, { Component } from 'react';
import {Button} from "reactstrap";
import './Home.css';
import {GenerateNewJson} from "../Mock/Mock";

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = { employees: [], loading: true, currentPage: 1, perPage: 5 };
        this.handleClick = this.handleClick.bind(this);
        this.updateButtonClick = this.updateButtonClick.bind(this);
    }

    handleClick(event) {
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    updateButtonClick() {
        this.updateData();
    }

    componentDidMount() {
        this.populateEmployeeData();
    }

    static renderForecastsTable(employees) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                <tr>
                    <th>Порядковый номер</th>
                    <th>Код</th>
                    <th>Значение</th>
                </tr>
                </thead>
                <tbody>
                {employees.map(employee =>
                    <tr key={Math.random()}>
                        <td>{employee.number}</td>
                        <td>{employee.code}</td>
                        <td>{employee.name}</td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }

    render() {
        const { employees, currentPage, perPage } = this.state;
        const pageNumbers = [];
        const indexOfLast = currentPage * perPage;
        const indexOfFirst = indexOfLast - perPage;
        const currentEmployees = employees.slice(indexOfFirst, indexOfLast);

        for (let i = 1; i <= Math.ceil(employees.length / perPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <li
                    key={number}
                    id={number}
                    onClick={this.handleClick}
                >
                    {number}
                </li>
            );
        });

        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Home.renderForecastsTable(currentEmployees);

        return (
            <div>
                <h1 id="tabelLabel" >Данные из БД</h1>
                <p>
                    <Button
                        onClick={this.updateButtonClick}>
                        Обновить данные в БД
                    </Button>
                </p>
                {contents}

                <ul id="page-numbers">
                    {renderPageNumbers}
                </ul>
            </div>
        );
    }

    async populateEmployeeData() {
        const response = await fetch('api/Employee');
        const data = await response.json();
        this.setState({ employees: data, loading: false });
    }

    async updateData() {
        let someJson = GenerateNewJson();

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(someJson)
        };
        const response = await fetch('api/Employee', requestOptions);
        if(response.status === 200)
        {
            await this.populateEmployeeData()
        }
    }
}